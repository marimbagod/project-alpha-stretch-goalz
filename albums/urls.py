from django.contrib import admin
from django.urls import path, include
from django.shortcuts import redirect


def redirect_to_songs(request):
    return redirect("display_song", id=0)



urlpatterns = [
    path("admin/", admin.site.urls),
    path("songs/", include("songs.urls")),
    path("", redirect_to_songs, name = "home"),
]
