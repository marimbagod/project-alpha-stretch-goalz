from django.urls import path
from songs.views import display_song, previous_song, next_song, song_details


urlpatterns = [
    path("<int:id>/", display_song, name="display_song"),
    path("<int:id>/previous", previous_song, name="previous_song"),
    path("<int:id>/next", next_song, name="next_song"),
    path("<int:id>/detail", song_details, name="song_details"),

]
