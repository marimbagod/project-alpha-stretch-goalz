from django.db import models
from django.contrib.auth.models import User

class Song(models.Model):
    title = models.CharField(max_length=200)
    album = models.CharField(max_length=200)
    picture = models.URLField(max_length=400)
    artist = models.CharField(max_length = 400)
    is_explicit = models.BooleanField(default = False)
    genre_choices = (
        ("pop", "Pop"),
        ("hiphop", "Hip Hop"),
        ("rnb", "RnB"),
        ("electronic", "Electronic"),
        ("rock", "Rock"),
        ("latin", "Latin"),
        ("country", "Country"),
        ("alternative", "Alternative"),
        ("metal", "Metal"),
        ("korean", "Korean"),
        ("classical", "Classical"),
        ("other", "Other"),
    )
    genre = models.CharField(max_length = 11, choices = genre_choices, default = "other")
    owner = models.ForeignKey(
        User,
        related_name="songs",
        on_delete=models.CASCADE,
        null=True,
    )

    def __str__(self):
        return self.title
