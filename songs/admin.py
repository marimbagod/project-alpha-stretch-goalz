from django.contrib import admin
from songs.models import Song

@admin.register(Song)
class SongAdmin(admin.ModelAdmin):
    list_display = ["title", "album", "picture", "artist", "is_explicit", "genre"]
