from django.shortcuts import render, get_object_or_404, redirect
from songs.models import Song

def display_song(request, id):
    songs = Song.objects.all()
    current_song = songs[id]
    return render(request, 'songs/current.html', {
        'current_song': current_song,
        'id': id,
        'total_songs': len(songs)
        })

def previous_song(request, id):
    songs = Song.objects.all()
    song_index = (id - 1) % len(songs)
    return redirect('display_song', id=song_index)

def next_song(request, id):
    songs = Song.objects.all()
    song_index = (id + 1) % len(songs)
    return redirect('display_song', id=song_index)

def song_details(request, id):
    song = get_object_or_404(Song, id=id)
    context = {
        "current_song" : song
    }
    return render(request, "songs/detail.html", context)
